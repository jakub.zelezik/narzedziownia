/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/



var users = [
  {
    id: '1',
    name: 'Jan',
    surname: 'Kowalski',
    email: 'adres.email@example.com',
    perm: 'User'
  },
  {
    id: '2',
    name: 'Stefan',
    surname: 'Kowalski',
    email: 'adres.email@example.com',
    perm: 'User'
  },
  {
    id: '3',
    name: 'Janusz',
    surname: 'Kowalski',
    email: 'adres.email@example.com',
    perm: 'Admin'
  },
];

var perms = [
  {
    id: '1',
    name: 'User'
  },
  {
    id: '2',
    name: 'Admin'
  }
];

var locations = [
	{		
		id: '1',
		name: "Siedziba, Dział DMO",
		type: "Wewnętrzna",
		street: "Walerego Sławaka 8",
    post_code: "00-000",
    city: "Kraków"
	},
	{		
		id: '2',
		name: "Magazyn, Dział DMO",
		type: "Wewnętrzna",
		street: "Żniwna 1",
		post_code: "00-000",
    city: "Kraków"
	},
	{		
		id: '3',
		name: "Siedziba, Dział DST",
		type: "Wewnętrzna",
		street: "Walerego Sławaka 8",
		post_code: "00-000",
    city: "Kraków"
	},
	{		
		id: '4',
		name: "Siedziba, Dział DPS",
		type: "Wewnętrzna",
		street: "Walerego Sławaka 8",
		post_code: "00-000",
    city: "Kraków"
	}
];

var items = [
  {
    id: '1',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-546567-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  },
  {
    id: '2',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-3245645-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  },
  {
    id: '3',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-23453425-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  },
  {
    id: '4',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-2345345-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  },
  {
    id: '5',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-2345435-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  },
  {
    id: '6',
    name: 'Super Turbo Mega Miernik 100G',
    part_number: 'FSG-34534523-DSF',
    serial_number: 'FDS234234234',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
  }
]

var operations = [
  {
    id: '1',
    from: users[0],
    to: users[1],
    location: locations[0],
    item: items[1],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  },
]


var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

app.get("/lokalizacje", (req, res, next) => {
	res.json(locations);
});

app.get("/lokalizacje/:id", (req, res, next) => {
	let id = req.params.id;
	res.json(locations.find(el => el.id == id));
});

app.get("/operacje", (req, res, next) => {
	res.json(operations);
});

app.get("/operacje/:id", (req, res, next) => {
	let id = req.params.id;
	res.json(operations.find(el => el.id == id));
});

app.get("/wyposazenie", (req, res, next) => {
  if(String(req.params.lokalizacja).length > 0){
    res.json([items[0], items[1]]);
  } else {
    res.json(items);
  }
});

app.get("/wyposazenie/:id", (req, res, next) => {
	let id = req.params.id;
	res.json(items.find(el => el.id == id));
});

app.get("/osoby", (req, res, next) => {
  res.json(users);
});

app.post("/osoby", async(req, res, next) => {
  res.json({status: 'ok'});
  res.status(200);
});

app.get("/osoby/uprawnienia", (req, res, next) => {
  res.json(perms);
});

app.get("/osoby/uprawnienia/:id", (req, res, next) => {
	let id = req.params.id;
	res.json(perms.find(el => el.id == id));
});

app.get("/osoby/:id", (req, res, next) => {
	let id = req.params.id;
	res.json(users.find(el => el.id == id));
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
