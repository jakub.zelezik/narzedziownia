import "./registerServiceWorker";

import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./styles/globals.scss";
import "vue3-virtual-scroller/dist/vue3-virtual-scroller.css";

import { Loader } from "@/directives/Loader";
import { Logger } from "@/plugins/Logger";

import VueVirtualScroller from "vue3-virtual-scroller";

createApp(App)
  .use(store)
  .use(router)
  .directive("loader", Loader)
  .use(Logger)
  .use(VueVirtualScroller)
  .mount("#app");
