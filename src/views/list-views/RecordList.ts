import { Vue } from "vue-class-component";
import {
  ErrorRecord,
  LoadingState,
  ResponseBody,
  UserData,
} from "@/interfaces";
import Fuse from "fuse.js";

export default abstract class RecordList<R> extends Vue {
  public API_CALL!: () => Promise<R[]>;
  public errors: ErrorRecord[] = [];
  public status: LoadingState = LoadingState.loading;

  public fuseData: Fuse<R> = Object.freeze(new Fuse<R>([]));
  public rawData: R[] = [];
  public filteredData: R[] = [];

  public renderHack = true;

  public query = "";
  public field = "";

  mounted() {
    this.loadList();
  }

  public async loadList(): Promise<void> {
    this.status = LoadingState.loading;

    try {
      const data = await this.API_CALL();
      this.status = LoadingState.loaded;

      try {
        await this.onLoaded(data);
      } catch (e) {
        console.log(e);
      }
    } catch (error) {
      this.errors = error.errors;
      this.status = LoadingState.error;
    }
  }

  public abstract async onLoaded(data: R[]): Promise<void>;
  public abstract async downloadCsv(): Promise<void>;

  filter(): void {
    if (this.query != "") {
      try {
        this.filteredData = [];
        for (const el of this.fuseData.search(this.query)) {
          this.filteredData.push(el.item);
        }
      } catch (e) {
        this.filteredData = [];
      }
    } else {
      this.filteredData = JSON.parse(JSON.stringify(this.rawData));
    }

    this.renderHack = false;
    this.$nextTick(() => {
      this.renderHack = true;
    });
    console.log(this.filteredData);
  }

  public async search(query: string, field: string): Promise<void> {
    console.log(query, field);
    this.query = query;
    this.field = field;

    this.filter();
  }
}
