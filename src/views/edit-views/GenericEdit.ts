import { Vue } from "vue-class-component";
import { LoadingState } from "@/interfaces";

export default abstract class GenericEdit<ListType, ElementType> extends Vue {
  public id!: number;

  public ELEMENT_API_CALL!: () => Promise<ElementType>;
  public LIST_API_CALL!: () => Promise<ListType[]>;

  public element_status: LoadingState = LoadingState.loading;
  public list_status: LoadingState = LoadingState.loading;

  mounted() {
    this.load_element();
    this.load_list();
  }

  private load_element = async () => {
    try {
      this.set_element(await this.ELEMENT_API_CALL());

      this.element_status = LoadingState.loaded;
    } catch (error) {
      this.element_status = LoadingState.error;
    }
  };

  private load_list = async () => {
    try {
      this.set_list(await this.LIST_API_CALL());

      this.list_status = LoadingState.loaded;
    } catch (error) {
      this.list_status = LoadingState.error;
    }
  };

  abstract set_element(data: ElementType): void;
  abstract set_list(list: ListType[]): void;
}
