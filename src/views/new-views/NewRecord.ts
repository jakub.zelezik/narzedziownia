import { Vue } from "vue-class-component";
import { ErrorRecord, ResponseBody } from "@/interfaces";
import { useRouter } from "vue-router";

export enum FormStatus {
  waiting,
  sending,
  error,
  sent,
}

export default abstract class NewRecord<S, R> extends Vue {
  public API_CALL!: (data: S) => Promise<void>;
  public BACK!: string;

  public errors: ErrorRecord[] = [];

  public status: FormStatus = FormStatus.waiting;
  public formStatus = FormStatus;

  public router = useRouter();

  public abstract get_data(): S;

  public get_error_for = (name: string): string | null => {
    const errors: ErrorRecord[] = this.errors.filter(
      (e: ErrorRecord) => e.field == name
    );

    if (errors.length > 0) {
      return errors[0].error;
    } else {
      return null;
    }
  };

  public on_submit = async (e: any) => {
    this.status = FormStatus.sending;

    try {
      await this.API_CALL(this.get_data());

      this.status = FormStatus.sent;
      this.$emit("model-reload");
      try {
        await this.on_sent();
      } catch (e) {
        console.log(e);
      }

      //setTimeout(() => {this.router.push(this.BACK)}, 1000);
    } catch (error) {
      this.errors = error.errors;
      this.status = FormStatus.error;
    }
  };

  public on_sent = async () => {
    console.log("loaded");
  };
}
