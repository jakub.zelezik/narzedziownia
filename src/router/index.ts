import {
  createRouter,
  createWebHistory,
  NavigationGuardNext,
  RouteLocationNormalized,
  RouteRecordRaw,
} from "vue-router";
import { NApi } from "@/interfaces/connector";

const logedInGuard = (
  from: RouteLocationNormalized,
  to: RouteLocationNormalized,
  next: NavigationGuardNext
) => {
  try {
    NApi.getCurrentUserId();
    next();
  } catch (e) {
    next("/logowanie");
  }
};

const logedInAdminGuard = (
  from: RouteLocationNormalized,
  to: RouteLocationNormalized,
  next: NavigationGuardNext
) => {
  try {
    if (NApi.getCurrentUserPermId() < 2) {
      throw "normal user";
    }
    next();
  } catch (e) {
    next(false);
  }
};

const routes: Array<RouteRecordRaw> = [
  {
    path: "/aplikacja",
    component: () =>
      import(/* webpackChunkName: "Nav" */ "../views/NavigationView.vue"),
    beforeEnter: logedInGuard,
    children: [
      {
        path: "/do_potwierdzenia",
        component: () =>
          import(
            /* webpackChunkName: "ToBeAccepted" */ "../views/list-views/ToBeAccepted.vue"
          ),
        beforeEnter: logedInGuard,
      },
      {
        path: "/wyposazenie",
        component: () =>
          import(
            /* webpackChunkName: "Items" */ "../views/list-views/Items.vue"
          ),
        beforeEnter: logedInGuard,
        children: [
          {
            path: "/wyposazenie/nowy",
            alias: ["/wyposazenie/nowa", "/wyposazenie/nowe"],
            component: () =>
              import(
                /* webpackChunkName: "Items" */ "../views/new-views/NewItem.vue"
              ),
            beforeEnter: logedInAdminGuard,
          },
        ],
      },
      {
        path: "/wyposazenie/:id",
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "Items" */ "../views/edit-views/EditItem.vue"
          ),
        beforeEnter: logedInGuard,
      },
      {
        path: "/lokalizacje",
        component: () =>
          import(
            /* webpackChunkName: "Locations" */ "../views/list-views/Locations.vue"
          ),
        beforeEnter: logedInGuard,
        children: [
          {
            path: "/lokalizacje/nowy",
            alias: ["/lokalizacje/nowa", "/lokalizacje/nowe"],
            component: () =>
              import(
                /* webpackChunkName: "Locations" */ "../views/new-views/NewLocation.vue"
              ),
            beforeEnter: logedInAdminGuard,
          },
        ],
      },
      {
        path: "/lokalizacje/:id",
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "Locations" */ "../views/edit-views/EditLocation.vue"
          ),
        beforeEnter: logedInGuard,
      },
      {
        path: "/osoby",
        component: () =>
          import(
            /* webpackChunkName: "Users" */ "../views/list-views/Users.vue"
          ),
        beforeEnter: logedInGuard,
        children: [
          {
            path: "/osoby/nowy",
            alias: ["/osoby/nowa", "/osoby/nowe"],
            component: () =>
              import(
                /* webpackChunkName: "Users" */ "../views/new-views/NewUser.vue"
              ),
            beforeEnter: logedInAdminGuard,
          },
        ],
      },
      {
        path: "/osoby/:id",
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "Users" */ "../views/edit-views/EditUser.vue"
          ),
        beforeEnter: logedInGuard,
      },
      {
        path: "/operacje",
        component: () =>
          import(
            /* webpackChunkName: "Actions" */ "../views/list-views/Actions.vue"
          ),
        beforeEnter: logedInGuard,
        children: [
          {
            path: "/operacje/nowy",
            alias: ["/operacje/nowa", "/operacje/nowe"],
            component: () =>
              import(
                /* webpackChunkName: "Actions" */ "../views/new-views/NewAction.vue"
              ),
            beforeEnter: logedInAdminGuard,
          },
        ],
      },
      {
        path: "/operacje/:id",
        props: true,
        component: () =>
          import(
            /* webpackChunkName: "Actions" */ "../views/edit-views/EditAction.vue"
          ),
        beforeEnter: logedInGuard,
      },
    ],
  },
  {
    path: "/logowanie",
    component: () =>
      import(/* webpackChunkName: "Login" */ "../views/LoginView.vue"),
    beforeEnter: (from, to, next) => {
      try {
        NApi.getCurrentUserId();
        next("/osoby");
      } catch (e) {
        next();
      }
    },
  },
  {
    path: "/haslo/:user_id/:auth_token",
    props: true,
    component: () =>
      import(/* webpackChunkName: "Password" */ "../views/SetPasswordView.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/logowanie",
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
