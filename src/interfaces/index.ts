export interface DataId {
  id: number;
}

export interface NewLocationData {
  name: string;
  type_id: number;
  street: string;
  post_code: string;
  city: string;
}

export interface LocationData extends DataId {
  name: string;
  type: string;
  street: string;
  post_code: string;
  city: string;
}

export interface NewItemData {
  name: string;
  part_number: string;
  serial_number: string;
  description: string;
  location_id: number;
}

export interface ItemData extends DataId {
  name: string;
  part_number: string;
  serial_number: string;
  description?: string;
  creation_date: string;
}

export interface NewUserData {
  name: string;
  surname: string;
  email: string;
  perm_id: number;
  password?: string;
}

export interface UserData extends DataId {
  name: string;
  surname: string;
  email: string;
  perm: string;
}

export enum LoadingState {
  loading = 0,
  loaded = 1,
  error = 2,
}

export interface NewActionData {
  from?: number;
  to?: number;
  location: number;
  item: number;
  description: string;
}

export interface ActionData extends DataId {
  from?: UserData;
  to?: UserData;
  location: LocationData;
  item: ItemData;
  description: string;
  confirmed: boolean;
  creation_date: Date;
}

export interface PermData extends DataId {
  name: string;
}

export interface LocationTypeData extends DataId {
  name: string;
}

export interface InputListItem {
  id: number;
  display_name: string;
}

export enum ResponseStatus {
  ok = "ok",
  authError = "authError",
  dataError = "dataError",
  internalError = "internalError",
}

export interface ErrorRecord {
  field: string | null;
  error: string;
}

export interface ResponseBody<T> {
  status: ResponseStatus;
  data?: T;
  token?: string;
  errors?: ErrorRecord[];
}

export class CsvDownload {
  private readonly determiner!: string;

  public constructor(determiner: string) {
    this.determiner = determiner;
  }

  public async download(jsonList: any[]): Promise<void> {
    if (jsonList.length > 0) {
      let csvText = "";
      const keys: string[] = [];

      for (const key of Object.keys(jsonList[0])) {
        keys.push(key);
      }

      csvText += `${keys.join(this.determiner)}\n`;

      for (const line of jsonList) {
        const values: string[] = [];

        for (const key of keys) {
          values.push(String(line[key]).replace(this.determiner, ""));
        }

        csvText += `${values.join(this.determiner)}\n`;
      }

      const filename = "export.csv";
      const blob = new Blob([new Uint8Array([0xef, 0xbb, 0xbf]), csvText], {
        type: "text/csv;charset=utf-8;",
      });
      const link = document.createElement("a");

      link.setAttribute("href", URL.createObjectURL(blob));
      link.setAttribute("download", filename);

      link.style.visibility = "hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

      console.log(csvText);
    }
  }
}
