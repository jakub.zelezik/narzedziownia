import {
  ActionData,
  ErrorRecord,
  ItemData,
  LocationData,
  LocationTypeData,
  NewActionData,
  NewItemData,
  NewLocationData,
  NewUserData,
  PermData,
  ResponseBody,
  ResponseStatus,
  UserData,
} from "@/interfaces/index";

export class NApiError {
  public errors!: ErrorRecord[];

  constructor(errors: ErrorRecord[] | undefined) {
    if (errors != undefined) {
      this.errors = errors;
    } else {
      this.errors = [];
    }
  }
}

export class NApiDataError extends NApiError {
  constructor(errors: ErrorRecord[] | undefined) {
    super(errors);
  }
}
export class NApiAuthError extends NApiError {
  constructor(errors: ErrorRecord[] | undefined) {
    super(errors);
  }
}
export class NApiInternalError extends NApiError {
  constructor(errors: ErrorRecord[] | undefined) {
    super(errors);
  }
}
export class NApiConnectionError extends NApiError {
  constructor() {
    super([
      {
        field: null,
        error:
          "Podczas łączenia z serwerem wystąpił błąd. Spróbuj ponownie później.",
      },
    ]);
  }
}

enum FilterType {
  equal = "=",
  grater = ">",
  smaller = "<",
}

export class QueryFilter {
  public rules: {
    field: string;
    type: FilterType | string;
    value: string | number;
  }[] = [];

  public isEqual(field: string, value: number | string): QueryFilter {
    this.rules.push({
      field,
      type: FilterType.equal,
      value,
    });

    return this;
  }

  public isGraterThen(field: string, value: number | string): QueryFilter {
    this.rules.push({
      field,
      type: FilterType.grater,
      value,
    });

    return this;
  }

  public is(
    filterName: string,
    field: string,
    value: number | string
  ): QueryFilter {
    this.rules.push({
      field,
      type: filterName,
      value,
    });

    return this;
  }

  public serialize(): string {
    return encodeURIComponent(btoa(JSON.stringify(this.rules)));
  }
}

export class NApi {
  private static BASE_URL = "https://backend.sal.test-project-domain.com";

  private static async doRequest(
    method: "GET" | "POST" | "HEAD",
    path: string,
    payload: any | undefined
  ): Promise<any> {
    try {
      const token: string | null = window.localStorage.getItem("token");

      const fetchInit: RequestInit = {
        method,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        credentials: "omit",
      };

      if (method != "GET" && method != "HEAD") {
        fetchInit.body = JSON.stringify({ data: payload });
      }

      const response = await fetch(`${NApi.BASE_URL}${path}`, fetchInit);

      if (response.status < 500) {
        const jsonResponse: ResponseBody<any> = await response.json();

        if (jsonResponse.status == ResponseStatus.ok) {
          if (jsonResponse.token) {
            window.localStorage.setItem("token", jsonResponse.token);
          }
          return jsonResponse.data;
        } else {
          switch (jsonResponse.status) {
            case ResponseStatus.authError:
              throw new NApiAuthError(jsonResponse.errors);
            case ResponseStatus.dataError:
              throw new NApiDataError(jsonResponse.errors);
            case ResponseStatus.internalError:
              throw new NApiInternalError(jsonResponse.errors);
          }
        }
      } else {
        throw new NApiInternalError([
          {
            field: null,
            error:
              "Wystąpił wewnętrzny błąd serwera. Spróbuj ponownie później.",
          },
        ]);
      }
    } catch (error) {
      console.error(error);
      if (error instanceof NApiError) {
        throw error;
      } else {
        throw new NApiConnectionError();
      }
    }
  }

  private static async doGetRequest(
    path: string,
    filter: QueryFilter
  ): Promise<any> {
    return await NApi.doRequest(
      "GET",
      `${path}?filter=${filter.serialize()}`,
      undefined
    );
  }

  private static async doPostRequest(path: string, payload: any): Promise<any> {
    return await NApi.doRequest("POST", path, payload);
  }

  public static async getUsers(
    filter: QueryFilter = new QueryFilter()
  ): Promise<UserData[]> {
    return await NApi.doGetRequest(`/users`, filter);
  }

  public static async getUser(
    id: number | undefined = undefined
  ): Promise<UserData> {
    if (id == undefined) {
      id = NApi.getCurrentUserId();
    }

    return await NApi.doGetRequest(`/users/${id}`, new QueryFilter());
  }

  public static getCurrentUserId(): number {
    const token: string | null = window.localStorage.getItem("token");
    if (token != null) {
      const payload: any = JSON.parse(atob(token.split(".")[1]));
      return payload.id;
    } else {
      throw new NApiAuthError([
        {
          field: null,
          error: "Token nie istnieje",
        },
      ]);
    }
  }

  public static getCurrentUserPermId(): number {
    const token: string | null = window.localStorage.getItem("token");
    if (token != null) {
      const payload: any = JSON.parse(atob(token.split(".")[1]));
      return payload.perm_id;
    } else {
      throw new NApiAuthError([
        {
          field: null,
          error: "Token nie istnieje",
        },
      ]);
    }
  }

  public static async newUser(data: NewUserData): Promise<void> {
    await NApi.doPostRequest("/users/new", data);
  }

  public static async getPerms(
    filter: QueryFilter = new QueryFilter()
  ): Promise<PermData[]> {
    return await NApi.doGetRequest(`/perms`, filter);
  }

  public static async getLocations(
    filter: QueryFilter = new QueryFilter()
  ): Promise<LocationData[]> {
    return await NApi.doGetRequest(`/locations`, filter);
  }

  public static async getLocation(id: number): Promise<LocationData> {
    return await NApi.doGetRequest(`/locations/${id}`, new QueryFilter());
  }

  public static async newLocation(data: NewLocationData): Promise<void> {
    await NApi.doPostRequest("/locations/new", data);
  }

  public static async getLocationTypes(
    filter: QueryFilter = new QueryFilter()
  ): Promise<LocationTypeData[]> {
    return await NApi.doGetRequest("/locations/types", filter);
  }

  public static async getItems(
    filter: QueryFilter = new QueryFilter()
  ): Promise<ItemData[]> {
    return await NApi.doGetRequest(`/items`, filter);
  }

  public static async getItem(id: number): Promise<ItemData> {
    return await NApi.doGetRequest(`/items/${id}`, new QueryFilter());
  }

  public static async newItem(data: NewItemData): Promise<void> {
    return await NApi.doPostRequest("/items/new", data);
  }

  public static async getActions(
    filter: QueryFilter = new QueryFilter()
  ): Promise<ActionData[]> {
    return await NApi.doGetRequest(`/actions`, filter);
  }

  public static async acceptAction(data: { id: number }): Promise<void> {
    await NApi.doPostRequest("/actions/accept", data);
  }

  public static async getUnacceptedActions(
    userId: number | undefined = undefined
  ): Promise<ActionData[]> {
    if (userId == undefined) {
      userId = NApi.getCurrentUserId();
    }

    const filter = new QueryFilter();
    filter
      .isEqual("to_id", parseInt(String(userId)))
      .is("unaccepted", "empty", 0);

    return await NApi.doGetRequest(`/actions`, filter);
  }

  public static async newAction(data: NewActionData): Promise<void> {
    await NApi.doPostRequest("/actions/new", data);
  }

  public static async login(data: {
    email: string;
    password: string;
  }): Promise<void> {
    await NApi.doPostRequest("/auth/login", data);
  }
}
