-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

USE Narzedziownia;
GO

-- [lokalizacja_typ]

CREATE TABLE [lokalizacja_typ]
(
 [loktyp_id]  int NOT NULL IDENTITY ,
 [loktyp_kod] varchar(50) NOT NULL ,


 CONSTRAINT [PK_lokalizacja_typ] PRIMARY KEY CLUSTERED ([loktyp_id] ASC)
);
GO


-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- [lokalizacja]

CREATE TABLE [lokalizacja]
(
 [lok_id]    int NOT NULL IDENTITY ,
 [lok_kod]   varchar(50) NOT NULL ,
 [lok_nazwa] varchar(100) NOT NULL ,
 [loktyp_id] int NOT NULL ,


 CONSTRAINT [PK_lokalizacja] PRIMARY KEY CLUSTERED ([lok_id] ASC),
 CONSTRAINT [FK_48] FOREIGN KEY ([loktyp_id])  REFERENCES [lokalizacja_typ]([loktyp_id])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_48] ON [lokalizacja] 
 (
  [loktyp_id] ASC
 )

GO










-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- [uprawnienia]

CREATE TABLE [uprawnienia]
(
 [upr_id]  int NOT NULL IDENTITY ,
 [upr_nazwa] varchar(50) NOT NULL ,


 CONSTRAINT [PK_uprawnienia] PRIMARY KEY CLUSTERED ([upr_id] ASC)
);
GO





-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- [osoby]

CREATE TABLE [osoby]
(
 [os_id]       int NOT NULL IDENTITY ,
 [os_imie]     varchar(50) NOT NULL ,
 [os_nazwisko] varchar(50) NOT NULL ,
 [os_email] 	 varchar(50) NOT NULL ,
 [os_haslo]    varchar(50) NOT NULL ,
 [upr_id]      int NOT NULL ,


 CONSTRAINT [PK_osoby] PRIMARY KEY CLUSTERED ([os_id] ASC),
 CONSTRAINT [FK_24] FOREIGN KEY ([upr_id])  REFERENCES [uprawnienia]([upr_id])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_24] ON [osoby] 
 (
  [upr_id] ASC
 )

GO









-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- [wyposazenie]

CREATE TABLE [wyposazenie]
(
 [wypos_id]       int NOT NULL IDENTITY ,
 [wypos_name]     varchar(50) NOT NULL ,
 [wypos_opis]     varchar(300) NOT NULL ,
 [wypos_datadod]  datetime NOT NULL ,
 [wypos_numerser] varchar(50) NOT NULL ,
 [wypos_numerkat] varchar(50) NOT NULL ,


 CONSTRAINT [PK_wyposazenie] PRIMARY KEY CLUSTERED ([wypos_id] ASC)
);
GO







-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- [operacje]

CREATE TABLE [operacje]
(
 [oper_id]     int NOT NULL IDENTITY ,
 [oper_data]   datetime NOT NULL ,
 [oper_opis]   varchar(300) NOT NULL ,
 [wypos_id]    int NOT NULL ,
 [lok_id]      int NOT NULL ,
 [os_idwydaj]  int NULL ,
 [os_idprzyjm] int NULL ,
 [oper_potw]   bit NULL ,


 CONSTRAINT [PK_operacje] PRIMARY KEY CLUSTERED ([oper_id] ASC),
 CONSTRAINT [FK_33] FOREIGN KEY ([wypos_id])  REFERENCES [wyposazenie]([wypos_id]),
 CONSTRAINT [FK_36] FOREIGN KEY ([lok_id])  REFERENCES [lokalizacja]([lok_id]),
 CONSTRAINT [FK_73] FOREIGN KEY ([os_idwydaj])  REFERENCES [osoby]([os_id])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_33] ON [operacje] 
 (
  [wypos_id] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_36] ON [operacje] 
 (
  [lok_id] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_73] ON [operacje] 
 (
  [os_idwydaj] ASC
 )

GO















